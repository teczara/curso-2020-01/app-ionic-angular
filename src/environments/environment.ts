// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDK13q8rJbiWphv2nCDf52mh63_u--V_P8",
    authDomain: "curso-teczara.firebaseapp.com",
    projectId: "curso-teczara",
    storageBucket: "curso-teczara.appspot.com",
    messagingSenderId: "425025090593",
    appId: "1:425025090593:web:4e6ecf2719db20ca2e5f60"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
