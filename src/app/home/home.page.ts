import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuariosService } from '../services/usuarios.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private cantidad: number[] = [1,3,5,8];
  public verLista: boolean = true;
  public titulo: string = 'MI APP';
  public usuarios$: Observable<any>;

  constructor(private usuariosService: UsuariosService) {}

  ngOnInit() {
    this.usuarios$ = this.usuariosService.usuariosParaTodos$;
  }

  ionViewDidLeave() {
  }

  suma(a: number, b: number): number {
    return a + b;
  }

  onClick() {
  }


  onUpdate() {
    this.usuariosService.updateUsuarios();
  }
}
