import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';


import { ChatServiceService } from './../../services/chat-service.service';

@Component({
  selector: 'app-chat-federico',
  templateUrl: './chat-federico.page.html',
  styleUrls: ['./chat-federico.page.scss'],
})
export class ChatFedericoPage implements OnInit {

  chats$ = new Observable<any[]>();

  constructor(private chatService: ChatServiceService) {this.chats$ = this.chatService.getChats();}

  chat = {texto: ''};


  ngOnInit() {

  }

  sendChat() {
    // console.log(this.chatService.addChat(this.chat));
    console.log(this.chat.texto);
    console.log(this.chats$);
    this.chatService.addChat(this.chat);
    this.chat.texto = '';
  }
}
