import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatFedericoPage } from './chat-federico.page';

const routes: Routes = [
  {
    path: '',
    component: ChatFedericoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatFedericoPageRoutingModule {}
