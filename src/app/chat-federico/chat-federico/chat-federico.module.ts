import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatFedericoPageRoutingModule } from './chat-federico-routing.module';

import { ChatFedericoPage } from './chat-federico.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatFedericoPageRoutingModule
  ],
  declarations: [ChatFedericoPage]
})
export class ChatFedericoPageModule {}
