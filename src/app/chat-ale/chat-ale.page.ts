import { Component, OnInit } from '@angular/core';
import { ChatServiceService } from "../services/chat-service.service";
import { Router } from "@angular/router"
import { BehaviorSubject, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

interface Mensaje{
  texto : string,
  fecha : Date
}

@Component({
  selector: 'app-chat-ale',
  templateUrl: './chat-ale.page.html',
  styleUrls: ['./chat-ale.page.scss'],
})


export class ChatAlePage implements OnInit {

  public title = "Ale";
  public msg;
 
  public mensajes$ :any;
  public mensajes = [];

  constructor(
    private service : ChatServiceService,
    private router : Router
  ) {
    this.mensajes$ = service.getChats()
    /*
    service.getChats().subscribe(
      m =>{
        
        m.map(
          mensaje =>{
            this.mensajes.push(mensaje)
          }
        )
        
      }
    )
   */ 
   }


  ngOnInit() {

  }
  
  

  onBack(){
    this.router.navigateByUrl("/home");
  }

  onSendMessage(){
   this.service.addChat({
    texto: this.msg,
    fecha : Date.now()
   })
    
  }

}
