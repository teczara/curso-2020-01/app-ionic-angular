import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatAlePageRoutingModule } from './chat-ale-routing.module';

import { ChatAlePage } from './chat-ale.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatAlePageRoutingModule
  ],
  declarations: [ChatAlePage]
})
export class ChatAlePageModule {}
