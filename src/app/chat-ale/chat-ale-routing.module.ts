import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatAlePage } from './chat-ale.page';

const routes: Routes = [
  {
    path: '',
    component: ChatAlePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChatAlePageRoutingModule {}
