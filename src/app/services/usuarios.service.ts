import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { switchMap } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  apiUrl = 'http://localhost:3000/usuarios'

  public usuarios$ = new BehaviorSubject(null); //Observable y Observer
  public usuariosParaTodos$;

  constructor(
    private http: HttpClient,
    private db: AngularFirestore,
    private storage: AngularFireStorage,
  ) {
    this.usuariosParaTodos$ = this.usuarios$.pipe(
      switchMap(() => {
        return this.db.collection('usuarios').valueChanges();
      })
    );
  }

  updateUsuarios(){
    this.usuarios$.next('');
  }

  guardar(usuario, foto) {
    this.db.collection('usuarios').add({
      ...usuario
    });
    this.storage.ref(usuario.nick).putString(foto, firebase.storage.StringFormat.DATA_URL);
  }
}
