import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class ChatServiceService {

  constructor(
    private db: AngularFirestore
  ) { }

  ngOnInit() {
  }

  getChats() {
    return this.db.collection('chats', ref => ref.orderBy('fecha')).valueChanges();
  }

  addChat(chat) {
    return this.db.collection('chats').add({
      ...chat,
      fecha: firebase.firestore.FieldValue.serverTimestamp()
    });
  }
}
