import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuariosService } from '../services/usuarios.service';
import { Camera } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  public nick: string;
  public nombre: string;
  public imagen: string;

  constructor(
    private usuariosService: UsuariosService,
    private router: Router,
    private camera: Camera
  ) { }

  ngOnInit() {
  }

  guardar() {
    this.usuariosService.guardar({
      nombre: this.nombre,
      nick: this.nick
    }, this.imagen);
    this.router.navigateByUrl('/');
  }

  onCamara() {
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL
    }).then(
      (imagen) => this.imagen = 'data:image/jpeg;base64,' + imagen
    );
  }

}
